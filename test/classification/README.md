# MMLKG Classification Test

This repository contains a small artificial and multimodal dataset to test the feature encoders of the MMLKG in a binary classification setting. The graph connecting these features has been randomly generated, such that all information must come from the features.

## Getting Started

Since the model does not directly work on RDF data, the first step entails the creation of the necessary input files. This can be done by calling the *generateInput* script with the context and target splits are input. The context is expected to be in HDT format, whereas the splits are CSV files which map the entity to its class.

To generate the input files, run:

    python generateInput.py -c test/classification/context.hdt -ts test/classification/train.csv -ws test/classification/test.csv -vs test/classification/valid.csv -d test/classification/data/

Next, start the training process by calling the *node_classification* script with the directory housing the just-generated files as input:

    python node_classification.py -i test/classification/data/

The above call will require the preprocessing of the input data on every new run. Alternatively, the *mkdataset* can be used to create a HDF5 file of the preprocessed data, and which can be used as input instead.

To generate the HDF5 dataset file, run:

    python mkdataset.py -i test/classification/data/ -c config.json -o test/classification/data/

Finally, run the MMLKG with this file as input:

    python node_classification.py -i test/classification/data/dataset.h5 -c config.json

Please see the help functions (`--help`) of these scripts for more information and more options.
